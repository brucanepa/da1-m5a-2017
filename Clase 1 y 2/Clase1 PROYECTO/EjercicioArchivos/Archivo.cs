﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioArchivos
{
    public class Archivo
    {
        public string Nombre { get; set; }

        public ICollection<AccesoDirecto> Accesos { get; }

        public Archivo(string nombre)
        {
            this.Nombre = nombre;
        }

        public AccesoDirecto CrearLink(string nombreLink)
        {
            return null;
        }

    }
}
