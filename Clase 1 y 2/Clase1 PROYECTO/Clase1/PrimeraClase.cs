﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clase1
{
    public class PrimeraClase
    {
        private string nombre = "Juan";

        public static string texto = "Texto";
        public const string constante = "Texto Constante";
        protected string atributoProtected = "atributo protected";

        public int Edad { get; set; }
        public string Nombre
        {
            get
            {
                /*string retorno;
                if (string.IsNullOrEmpty(nombre))
                {
                    retorno = "vacio";
                } 
                else
                {
                    retorno = nombre;
                }

                return retorno;*/

                return string.IsNullOrEmpty(nombre) ? "vacio" : nombre;
            }
            set { }
        }

        public PrimeraClase()
        { }

        public PrimeraClase(string unNombre, int unaEdad)
        {
            nombre = unNombre;
            Edad = unaEdad;
        }

        public void ImprimirNombre(ref int numero)
        {
            numero = 50;
            Console.WriteLine(Nombre + numero);
        }

        public void ImprimirNombreOut(out int numero)
        {
            numero = 50;
            Console.WriteLine(Nombre + numero);
        }

        public override string ToString()
        {
            return "Nombre: " + Nombre + " Edad: " + Edad;
        }

        public override bool Equals(object obj)
        {
            bool equals = false;

            if (obj != null && this.GetType().Equals(obj.GetType()))
            {
                PrimeraClase otroObjeto = (PrimeraClase)obj;
                //PrimeraClase otroObjetoA = obj as PrimeraClase;
                equals = this.Nombre.Equals(otroObjeto.Nombre);
            }

            return equals;
        }

    }
}
