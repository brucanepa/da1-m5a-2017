﻿using Biblioteca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clase1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Console.WriteLine("Ingrese edad");
            string edad = Console.ReadLine();

            Console.WriteLine("Su edad es " + edad);*/

            /*string[] array = new string[5];
            string[] array2 = { "2", "4", "10", "12" };

            List<string> lista = new List<string>();
            lista.Add("primer elemento");
            lista.Add("segundo elemento");
            lista.Add("tercer elemento");

            Console.WriteLine("Foreach array");
            foreach (string elemento in array2)
            {
                Console.WriteLine(elemento);
            }

            Console.WriteLine("Foreach");
            foreach (string elemento in lista)
            {
                Console.WriteLine(elemento);
            }

            Console.WriteLine("For");
            for (int i = 0; i < lista.Count; i++)
            {
                Console.WriteLine(lista[i]);
            }

            // formatear codigo: ctrl + k + d
            */
            PrimeraClase pepe = new PrimeraClase("juan", 10);
            PrimeraClase juan = new PrimeraClase("juan", 20);

            Console.WriteLine(juan.Equals("hola"));
            Console.WriteLine(pepe.Equals(juan));
            Console.WriteLine(pepe.Equals(null));

            int valor = 10;
            int valorOut;
            juan.ImprimirNombre(ref valor);
            juan.ImprimirNombreOut(out valorOut);

            Console.WriteLine("valor " + valor);
            Console.WriteLine("valorOut " + valorOut);

            /*string nombre = juan.Nombre;

            Console.WriteLine(nombre);

            Console.WriteLine(PrimeraClase.texto);
            

            int valor = 10;
            juan.ImprimirNombre(ref valor);
            Console.WriteLine(valor);

            //MetodosDeString();
            MetodosInt();*/
            Console.ReadLine();
        }

        public static void CodigoOtroProyecto()
        {
            ClaseEnOtroProyecto clase = new ClaseEnOtroProyecto();
            clase.Nombre = "Nombre";
        }

        public static void MetodosDeString()
        {
            string espacios = "    ";
            string vacio = "";
            string lleno = "lleno";
            Console.WriteLine(string.IsNullOrEmpty(vacio));
            Console.WriteLine(string.IsNullOrWhiteSpace(vacio));
            Console.WriteLine();
            Console.WriteLine(string.IsNullOrEmpty(espacios));
            Console.WriteLine(string.IsNullOrWhiteSpace(espacios));
            Console.WriteLine();
            Console.WriteLine(string.IsNullOrEmpty(lleno));
            Console.WriteLine(string.IsNullOrWhiteSpace(lleno));

            Console.WriteLine("    holachau     ".Trim());
            Console.WriteLine("Contains" + "holachau".Contains("hola"));
            Console.WriteLine("Contains" + "   holachau".IndexOf("hola"));
            Console.WriteLine(string.Format("{0} hola {2}. ->{1}", "primero", "segundo", "tercero"));
        }

        public static void MetodosInt()
        {
            string numeroEnString = "hola";
            //Console.WriteLine(int.Parse(numeroEnString));
            int numero;
            int.TryParse(numeroEnString, out numero);
            Console.WriteLine(numero);
        }
    }
}
